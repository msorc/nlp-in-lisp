;; This software is Copyright (c) Gerald Gazdar and Chris Mellish, University of Sussex, 1989
;; Authors grants you the rights to distribute
;; and use this software as governed by the terms
;; of the Lisp Lesser GNU Public License
;; (http://opensource.franz.com/preamble.html),
;; known as the LLGPL.

;;; tdrecog.lsp [Chapter  5] top-down recognition for a CF-PSG

(defun next (goals string)
  (if (and (null goals) (null string))
    (print '(yes))
    (if (listp (car goals))
      (dolist (rule rules)
        (if (equal (car goals) (car rule))
          (next (append (cdr rule) (cdr goals)) string)))
      (if (equal (car goals) (car string))
        (next (cdr goals) (cdr string))))))
