;; This software is Copyright (c) Gerald Gazdar and Chris Mellish, University of Sussex, 1989
;; Authors grants you the rights to distribute
;; and use this software as governed by the terms
;; of the Lisp Lesser GNU Public License
;; (http://opensource.franz.com/preamble.html),
;; known as the LLGPL.

;;; finite.lsp [Chapter  2] utilities for finite state networks

;;; abbreviations

(defvar abbreviations)

;;; basic network accessing

(defun initial_nodes (network)
;;  returns the list of initial nodes
   (nth 1 (assoc 'Initial network)))

(defun final_nodes (network)
;;  returns the list of final nodes
   (nth 1 (assoc 'Final network)))

(defun transitions (network)
;;  returns the list of transitions
   (cddr network))

;;; the subcomponents of a transition

(defun trans_node (transition)
   (getf transition 'From))

(defun trans_newnode (transition)
   (getf transition 'to))

(defun trans_label (transition)
   (getf transition 'by))
