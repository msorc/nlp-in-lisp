;; This software is Copyright (c) Gerald Gazdar and Chris Mellish, University of Sussex, 1989
;; Authors grants you the rights to distribute
;; and use this software as governed by the terms
;; of the Lisp Lesser GNU Public License
;; (http://opensource.franz.com/preamble.html),
;; known as the LLGPL.

;;; cfpsgram.lsp [Chapter  4] example context free grammar in list format

(defvar rules)

(setq rules
 '(((S) (NP) (VP))
   ((VP) (V))
   ((VP) (V) (NP))
   ((V) died)
   ((V) employed)
   ((NP) nurses)
   ((NP) patients)
   ((NP) Medicenter)
   ((NP) Dr Chan)))
