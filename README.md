# nlp-in-lisp
## Origins
Lisp source code from [Gazdar, Mellish. Natural Language Processing in LISP](http://www.informatics.sussex.ac.uk/research/groups/nlp/gazdar/nlp-in-lisp/index.html) book.

On behalf of [Chris Mellish](http://homepages.abdn.ac.uk/c.mellish/pages/) original sources are shared under the terms of LLGPL.
